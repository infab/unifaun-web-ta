# Unifaun Web TA
A Laravel package for communicating with Unifaun Web TA

[![pipeline status](https://gitlab.com/infab/unifaun-web-ta/badges/master/pipeline.svg)](https://gitlab.com/infab/unifaun-web-ta/commits/master)
[![coverage report](https://gitlab.com/infab/unifaun-web-ta/badges/master/coverage.svg)](https://gitlab.com/infab/unifaun-web-ta/commits/master)